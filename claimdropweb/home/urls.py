from django.urls import path
from . import views 
from django.conf.urls import url
    
urlpatterns = [
        # path('', views.index),
        path('', views.snapshot),
        path('index/', views.index, name='index'),
        path('login/', views.signin , name='login'),
        path('logout/', views.signout, name='logout'),
        path('snapshot/', views.snapshot),
        url(r'api/snapshot$', views.snap),
        url(r'api/pca$', views.peerplays_create_account),
        url(r'api/transfer$', views.claim),
        url(r'api/transfer2pu$', views.claim_pu),
        # path('signup/', views.signup),
]   
